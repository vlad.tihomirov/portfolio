FROM node:18.12.1 AS app
WORKDIR /app
COPY package.json /app
RUN npm cache clean --force
RUN npm i
COPY . .
RUN npm install
RUN npm run build

FROM nginx:1.23.3-alpine AS nginx
COPY default.conf /etc/nginx/conf.d/
RUN rm -rf /usr/share/nginx/html/*
COPY --from=app /app/dist/  /usr/share/nginx/html/
EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]
